# OpenVent

This project is an unofficial client for the **Vent** REST API.

## API

### Reverse-Engineering (Android)

 1. Install and activate the **Xposed** module **JustTrustMe** to circumvent SSL certificate pinning.
 2. Go to your Internet settings and set up a proxy to your PCs IP and port.
 3. Set up **mitmproxy** to listen on said PC, at least these URLs should be ignored to make things easier:
    * `"certs.lyr8.net"`
    * `"res.cloudinary.com/vent/"`
    * `".*.googlesyndication.com"`
    * `".*.googleapis.com"`
    * `".*.google.com"`
    * `"gstatic.com"`
    * `".*.googleusercontent.com"`
    * `".*.google-analytics.com"`
 4. Go to your browser on your phone and go to <http://mitm.it>, install the certificate.
 5. Open Vent and observe the requests and responses in mitmproxy.
 
### Notes

The API uses **Retrofit** and **Gson** to communicate.
The Retrofit Vent API is described in `IVentService.java`, the Rest context is managed in `Context.java`.
Some calls do not only return a single entity (like a User) but a wrapper (WrapUser) that contains a reference to the
actual class and some metadata, that's why we need the classes in `api.internal.wrap`.

## Contributing

### Code Style

 * indentation width: Tabs for nesting, spaces for alignment ("smart tabs")
 * indentation: indent code blocks once, but not switch-case
 * line length: 120 cpl, assuming a tab being 2 spaces
 * braces: classes and similar on next line, methods/loops/... on the same line.
 
## ToDo

 * (G)UI
 * Unit-Tests