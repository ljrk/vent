package org.lrk.vent.api.internal;

import java.sql.Timestamp;
import java.util.UUID;

public class Notification {
	Timestamp createdAt;
	UUID id;
	Timestamp lastActivityAt;
	Message message;
	public static class Message {
		String body;
		String title;
	}
	Params params;
	public static class Params {
		Integer emotionCategeroyColor;
		UUID groupId;
		User user;
		String ventBody;
	}
	Boolean read;
	String type; // InteractedWithVent, MentionedInVent, FavouriteComment, MentionedInComment
}
