package org.lrk.vent.api.internal.wrap;

import org.lrk.vent.api.internal.Emotion;
import org.lrk.vent.api.internal.EmotionCategory;
import org.lrk.vent.api.internal.InteractionType;
import org.lrk.vent.api.internal.Vent;

import java.util.List;
import java.util.UUID;

public class WrapVents {
	List<Vent> vents;
	List<InteractionType> interactionTypes;
	List<Emotion> emotions;
	List<EmotionCategory> emotionCategories;
	Meta meta;

	public static class Meta {
		public UUID[] deletedIds;
		public int ventsPerAd;
	}
}
