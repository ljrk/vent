package org.lrk.vent.api.internal.wrap;

import org.lrk.vent.api.internal.SearchSuggestion;

public class WrapSearchSuggestions {
	Meta meta;

	public static class Meta {
		String[] deletedIds;
	}

	public SearchSuggestion[] searchSuggestions;
}
