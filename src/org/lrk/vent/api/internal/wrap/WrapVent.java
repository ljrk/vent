package org.lrk.vent.api.internal.wrap;

import org.lrk.vent.api.internal.Emotion;
import org.lrk.vent.api.internal.EmotionCategory;
import org.lrk.vent.api.internal.InteractionType;
import org.lrk.vent.api.internal.Vent;

public class WrapVent {
	EmotionCategory[] emotionCategories;
	Emotion[] emotions;
	Vent vent;
	InteractionType interactionTypes;
}
