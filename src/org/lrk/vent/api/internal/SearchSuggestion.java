package org.lrk.vent.api.internal;

import java.sql.Timestamp;
import java.util.UUID;

public class SearchSuggestion {
	Timestamp createdAt;
	public UUID id;
	Integer score;
	Timestamp updatedAt;
	public String value;
}
