package org.lrk.vent.api.internal;

import java.util.UUID;

public class WrapNotifications {
	Meta meta;
	public static class Meta {
		UUID[] deletedIds;
	}
	Notification[] notifications;
}
