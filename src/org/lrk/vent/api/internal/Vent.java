package org.lrk.vent.api.internal;

import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

public class Vent {
	Boolean authorIsFollowed;
	String body;
	String city;
	String commentSetting;
	Timestamp createdAt;
	UUID emotionId;
	String group; //TODO
	UUID groupdId; //TODO
	UUID id;
	Map<String, Integer> interactionCounts;
	Map<String, Integer> interactionFlags;
	Boolean isExplicit;
	Map<String, String> links;
	MentionedUser[] mentionedUsers;

	public static class MentionedUser {
		String placeholder;
		UUID userId;
		String username;
	}

	String privacySetting;
	Timestamp updatedAt;
	User user;

	public static class User {
		URL profileImageUrl;
		UUID userId;
		String username;
	}
}
