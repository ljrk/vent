package org.lrk.vent.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.lrk.vent.api.internal.IVentService;
import org.lrk.vent.api.internal.SearchSuggestion;
import org.lrk.vent.api.internal.SignIn;
import org.lrk.vent.api.internal.User;
import org.lrk.vent.api.internal.wrap.WrapSearchSuggestions;
import org.lrk.vent.api.internal.wrap.WrapUser;
import org.lrk.vent.api.internal.wrap.WrapUsers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

public class Context {
	final static Logger logger = Logger.getLogger(Context.class.getName());
	/*
	 * "X-User-Username"
	 * "X-User-Token"
	 * "X-3PO"
	 * "X-OB1"
	 * "X-User-Platform"
	 */
	protected final static String propertyFile = "config.properties";

	Properties credentials;
	IVentService service;

	public SearchSuggestion[] getSearchSuggestions(String query) {
		Call<WrapSearchSuggestions> sugg = service.getSearchSuggestions(
						credentials.getProperty("X-User-Username"),
						credentials.getProperty("X-User-Token"),
						credentials.getProperty("X-3PO"),
						credentials.getProperty("X-OB1"),
						credentials.getProperty("X-User-Platform"),
						5,
						query);

		try {
			return sugg.execute().body().searchSuggestions;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	public User[] searchUser(String query) {
		Call<WrapUsers> user = service.searchUser(
						credentials.getProperty("X-User-Username"),
						credentials.getProperty("X-User-Token"),
						credentials.getProperty("X-3PO"),
						credentials.getProperty("X-OB1"),
						credentials.getProperty("X-User-Platform"),
						5,
						query);
		try {
			Response<WrapUsers> wu = user.execute();
			return wu.body().users;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	public WrapUser getUserInfo(UUID ventUser) {
		Call<WrapUser> users = service.getUser(credentials.getProperty("X-User-Username"),
						credentials.getProperty("X-User-Token"),
						credentials.getProperty("X-3PO"),
						credentials.getProperty("X-OB1"),
						credentials.getProperty("X-User-Platform"),
						ventUser.toString() + ".json");
		try {
			return users.execute().body();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	{
		Gson gson = new GsonBuilder().setFieldNamingPolicy(
						FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
		//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.0.12", 8080));
		OkHttpClient client = new OkHttpClient.Builder()
						//.proxy(proxy)
						.addInterceptor(new LoggingInterceptor())
						.build();
		Retrofit retrofit = new Retrofit.Builder()
						.baseUrl("https://api.ventfeed.com/")
						.client(client)
						.addConverterFactory(GsonConverterFactory.create(gson))
						.build();
		service = retrofit.create(IVentService.class);
	}

	public Context() {
		try (FileInputStream config = new FileInputStream(propertyFile)) {
			credentials = new Properties();
			credentials.load(config);
		} catch (IOException ioe) {
			//ioe.printStackTrace();
			logger.info(String.format("%s not found, not logged in", propertyFile));
		}
	}

	public boolean SignedIn() {
		return (credentials != null);
	}

	public boolean signIn(String username, String password, String deviceId) {
		// "user": { "username": "abc", "password": "XXX" }
		Map<String, String> loginCredentials = new HashMap<>();
		loginCredentials.put("username", username);
		loginCredentials.put("password", password);
		Map<String, Map<String, String>> loginInformation = new HashMap<>();
		loginInformation.put("user", loginCredentials);

		Call<SignIn> signIn = service.signIn(loginInformation);

		try {
			final Response<SignIn> response = signIn.execute();
			SignIn data = response.body();
			credentials = new Properties();
			credentials.put("X-User-Username", data.user.username);
			credentials.put("X-User-Token", data.user.authenticationToken);
			String androidToken = "";
			String checkToken = "";
			try {
				MessageDigest instance = MessageDigest.getInstance("SHA-1");
				try {
					byte[] androidDigest = instance.digest((deviceId + "VentAndroidClient").getBytes("utf-8"));
					for (byte b : androidDigest) {
						androidToken = androidToken + (String.format("%02x", b));
					}
				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
					return false;
				}
				try {
					byte[] checkDigest = instance.digest((androidToken + "VFProfileRegisterEmailTableViewCell").getBytes("utf-8"));
					for (byte b : checkDigest) {
						checkToken = checkToken + (String.format("%02x", b));
					}
				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
					return false;
				}
			} catch (NoSuchAlgorithmException nsa) {
				nsa.printStackTrace();
				return false;
			}
			credentials.put("X-3PO", androidToken);
			credentials.put("X-OB1", checkToken);
			credentials.put("X-User-Platform", "android");

			try (FileOutputStream config = new FileOutputStream(propertyFile)) {
				credentials.store(config, "credentials");
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public Context(String username, String password, String deviceId) {
		signIn(username, password, deviceId);
	}

	private static class LoggingInterceptor implements Interceptor {
		@Override
		public okhttp3.Response intercept(Chain chain) throws IOException {
			Request request = chain.request();

			long t1 = System.nanoTime();
			logger.info(String.format("Sending request %s on connection %s%n==HEADERS==%n%s==BODY==%n%s%n==END==%n",
							request.url(), chain.connection(), request.headers(),
							new GsonBuilder().setPrettyPrinting().create().toJson(request.body())));

			okhttp3.Response response = chain.proceed(request);

			long t2 = System.nanoTime();
			response.body().source().request(Long.MAX_VALUE);
			logger.info(String.format("Received response for %s in %.1fms%n==HEADERS==%n%s==BODY==%n%s%n==END==%n",
							response.request().url(), (t2 - t1) / 1e6d, response.headers(),
							/* _UGLY_ hack: Convert Json to JsonObject and then back, this time using prettyPrint */
							new GsonBuilder().setPrettyPrinting().create().toJson(
											new JsonParser().parse(
															response.body().source().buffer().clone().readString(Charset.forName("utf-8")))
															.getAsJsonObject())
			));

			return response;
		}
	}
}
