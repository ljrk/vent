package org.lrk.vent.desktop;

import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.lrk.vent.api.Context;
import org.lrk.vent.api.internal.SearchSuggestion;
import org.lrk.vent.api.internal.User;


public class Main {
	public static void main(String args[]) {
		Context c = new Context();
		if (!c.SignedIn()) {
			c.signIn(args[0], args[1], args[2]);
		}

		LineReader lineReader = LineReaderBuilder.builder().build();
		while (true) {
			String line;
			try {
				line = lineReader.readLine("> ");
			} catch (UserInterruptException uie) {
				continue;
			} catch (EndOfFileException eofe) {
				return;
			}
			if (line.startsWith("search ")) {
				String query = line.substring(("search").length()+1);
				System.out.println("Searching for \""+query+"\":");
				for (User user : c.searchUser(query)) {
					System.out.println(user.username);
				}
			} /* else if ... */
			else {
				System.err.println("Invalid input!");
			}
		}
	}
}
